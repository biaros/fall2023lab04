package debugging.assignment;

public class Company {
    private Employee[] employees;

    public Company(Employee[] employees) { // must create a NEW employee object and assign values one by one
        this.employees = new Employee[employees.length];

        for (int i = 0; i < employees.length; i++)
        {
            // this.employees[i] = employees[i]; cannot do this because objects still have the same references: when one is changed so is the other, does NOT solve the problem
            this.employees[i] = new Employee(employees[i]);
        }
    }

    public Company(Company other)
    {
        this(other.employees);
    }

    /**
     * Exercise: Fill in the JavaDocs comment to describe what this method is supposed to do!
     * @param name
     * @return
     */
    public boolean doesWorkFor(String name) {
        for (Employee e : employees) {
            if (e.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Company)) {
            return false;
        }

        Company other = (Company) o;
        for (int i = 0; i < other.employees.length; i++) {
            if (!other.employees[i].equals(this.employees[i])) {
                return false;
            }
        }
        return true;
    }

}
